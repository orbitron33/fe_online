const API = "https://ajax.test-danit.com/api/swapi/films";
const container = document.querySelector(".posts-list");

const sendRequest = async (url) => {
  const response = await fetch(url);
  const result = await response.json();
  return result;
};

const renderPostFilms = (url) => {
  sendRequest(url).then((data) => {
    data.forEach(({ characters, episodeId, name, openingCrawl, id }) => {
      container.insertAdjacentHTML(
        "afterbegin",
        `<div class="film-content">
					<p class="film-episodeId">${episodeId}</p>
					<p class="film-name">${name}</p>
          <div id="${id}" class="film-actor"></div>
          <p class="film-openingCrawl">${openingCrawl}</p>
				</div>`
      );
      const film = document.getElementById(`${id}`);
      getCharacters(characters).then((res) => {
        film.innerHTML = res;
      });
    });
  });
};

const getCharacters = (characters) => {
  const actorsURL = characters;
  const requests = actorsURL.map((url) => fetch(url));
  return Promise.all(requests).then((responses) => {
    const results = responses.map((response) => response.json());
    return Promise.all(results).then((results) => {
      let nameActors = results
        .map((person) => `<span>${person.name}</span>`)
        .join();
      return nameActors;
    });
  });
};

renderPostFilms(API);
