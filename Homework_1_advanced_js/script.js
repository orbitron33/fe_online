class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }
  set name(name) {
    this._name = name;
  }
  set age(age) {
    this._age = age;
  }
  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const firstProgrammer = new Programmer("Andrii", "30", 600, [
  "Enlish",
  "Ukrainian",
  "Italian",
]);
const secondProgrammer = new Programmer("Yuriy", "32", 700, [
  "French",
  "Polish",
  "Italian",
]);
const thirdProgrammer = new Programmer("Irina", "28", 500, [
  "French",
  "Ukrainian",
  "German",
]);

console.log(firstProgrammer);
console.log(secondProgrammer);
console.log(thirdProgrammer);
