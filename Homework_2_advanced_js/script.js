const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const ulBox = document.querySelector("#root");
ulBox.insertAdjacentHTML("afterbegin", '<ul id="list"></ul>');
const list = document.querySelector("#list");
const keys = ["author", "name", "price"];

function createList(arr) {
  arr.forEach((book, index) => {
    let missedKey;
    const bookKeys = Object.keys(book);
    const isValid = keys.every((key) => {
      missedKey = key;
      return bookKeys.includes(key);
    });
    try {
      if (isValid) {
        list.insertAdjacentHTML(
          "beforeend",
          `<li><p>${book[keys[0]]}</p><p>${book[keys[1]]}</p><p>${
            book[keys[2]]
          }</p></li>`
        );
      } else {
        throw Error(`Object № ${index + 1} doesn't have key - ${missedKey}`);
      }
    } catch (e) {
      console.log(e);
    }
  });
}
createList(books);
