const API_USERS = "https://ajax.test-danit.com/api/json/users";
const API_POSTS = "https://ajax.test-danit.com/api/json/posts";
const container = document.querySelector(".content");

const sendRequest = async (url) => {
  const response = await fetch(url);
  const result = await response.json();
  return result;
};

const users = sendRequest(API_USERS).then((users) => users);
const posts = sendRequest(API_POSTS).then((posts) => posts);

const renderPosts = () => {
  return users.then((userData) => {
    userData.forEach(({ id, name, email }) => {
      posts.then((postsData) => {
        postsData.forEach(({ id: postId, userId, title, body }) => {
          if (id === userId) {
            const userCard = new Card(
              id,
              name,
              email,
              postId,
              userId,
              title,
              body
            );
            userCard.createCard();
          }
        });
      });
    });
  });
};

class Card {
  constructor(id, name, email, postId, userId, title, body) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.postId = postId;
    this.userId = userId;
    this.title = title;
    this.body = body;
  }
  createCard() {
    const del = document.createElement("div");
    del.classList.add(".twit-del");
    del.innerHTML = `<img id ="${this.postId}" src="./iconDel.png" alt="" />`;
    const post = document.createElement("div");
    post.classList.add("posts-list");
    post.id = `post${this.postId}`;
    post.append(del);
    post.insertAdjacentHTML(
      "afterbegin",
      `
                    <div class="logo">${this.name[0]}</div>
                    <div class="twit">
                      <h2 class="user-name">${this.name}</h2>
                      <span class="user-email">${this.email}</span>
                      <h3 class="twit-title">${this.title}</h3>
                      <p class="twit-text">${this.body}</p>
                    </div>
                
              `
    );
    container.append(post);
    del.addEventListener("click", () => {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
        method: "DELETE",
      })
        .then((response) => {
          if (response.status === 200) {
            post.remove();
          }
        })
        .catch((err) => console.log(err));
    });
  }
}
renderPosts();
