const API_IP = "https://api.ipify.org/?format=json";
const API_DATA = "http://ip-api.com/";
const container = document.querySelector(".wrapper");
const userIp = document.getElementById("ip");
const userInfo = document.getElementById("info");
const getIpBtn = document.getElementById("get-ip");
const searchUser = document.getElementById("search");
let ip;
// const sendRequest = async (url) => {
//   const response = await fetch(url);
//   const result = await response.json();
//   return result;
// };

getIpBtn.addEventListener("click", () => {
  sendRequest(API_IP).then((res) => {
    userIp.innerText = res.ip;
    ip = res.ip;
  });
});

searchUser.addEventListener("click", () => {
  sendRequest(
    `${API_DATA}json/${ip}?fields=continent,country,regionName,city,district`
  ).then(({ continent, country, regionName, city, district }) => {
    userInfo.insertAdjacentHTML(
      "afterbegin",
      `
        <p class="continent">Continent: ${continent}</p>
        <p class="country">Country: ${country}</p>
        <p class="region">${regionName}</p>
        <p class="city">City: ${city}</p>
        <p class="district">District: ${district}</p>
      `
    );
  });
});

const sendRequest = async (url, method = "GET", config) => {
  return await fetch(url, {
    method,
    ...config,
  }).then((response) => {
    if (response.ok) {
      if (method === "GET" || method === "POST" || method === "PUT") {
        return response.json();
      }
      return response;
    } else {
      return new Error("error");
    }
  });
};
