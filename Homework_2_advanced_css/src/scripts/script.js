const menuBtn = document.querySelector("#burgerBtn");
const btnImg = document.querySelector("#burgerImg");
const navigation = document.querySelector(".header__navigation");

menuBtn.addEventListener("click", () => {
  if (btnImg.src.includes("open")) {
    btnImg.src = `images/menu-button-close.png`;
    navigation.style.display = "block";
  } else {
    btnImg.src = `images/menu-button-open.png`;
    navigation.style.display = "none";
  }
});
