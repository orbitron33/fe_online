const { series, watch } = require("gulp");
const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const autoprefixer = require("gulp-autoprefixer");
const clean = require("gulp-clean");
const concat = require("gulp-concat");
const cleanCSS = require("gulp-clean-css");
const csso = require("gulp-csso");
const imagemin = require("gulp-imagemin");
const { renderSync } = require("sass");
const jsMin = require("gulp-js-minify");
const rename = require("gulp-rename");
const sync = require("browser-sync").create();

let buildStyles = () => {
  return gulp
    .src("./src/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(csso())
    .pipe(concat("style.min.css"))
    .pipe(cleanCSS())
    .pipe(gulp.dest("./dist/css"));
};

let compressImg = () => {
  return gulp
    .src("./src/images/*.png")
    .pipe(imagemin())
    .pipe(gulp.dest("dist/images"));
};

let minifyJs = () => {
  return gulp
    .src("./src/**/*.js")
    .pipe(jsMin())
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest("dist"));
};

let clearDist = () => {
  return gulp.src("dist/*", { read: false }).pipe(clean());
};

let copyCss = () => {
  return gulp
    .src("./src/styles/**/*.css")
    .pipe(rename({ dirname: "" }))
    .pipe(gulp.dest("dist/css"));
};

let copyHtml = () => {
  return gulp.src("./*.html").pipe(gulp.dest("dist/"));
};

let copyFonts = () => {
  return gulp.src("./src/styles/**/*.woff2").pipe(gulp.dest("dist/"));
};

let serve = () => {
  sync.init({
    server: "./dist",
  });
  watch("./src/**/*.scss", series(buildStyles)).on("change", sync.reload);
  watch("./src/**/*.js", series(minifyJs)).on("change", sync.reload);
};

exports.build = series(
  clearDist,
  compressImg,
  copyCss,
  copyFonts,
  copyHtml,
  minifyJs,
  buildStyles
);
exports.dev = series(minifyJs, buildStyles, serve);
